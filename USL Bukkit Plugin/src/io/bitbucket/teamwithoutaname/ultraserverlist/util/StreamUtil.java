package io.bitbucket.teamwithoutaname.ultraserverlist.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StreamUtil {

	public static String convertInputStreamToString(InputStream stream) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
		
		String line;
		StringBuffer response = new StringBuffer();
		while((line = bufferedReader.readLine()) != null) {
			response.append(line);
		}
		bufferedReader.close();
		
		return response.toString();
	}
	
}
