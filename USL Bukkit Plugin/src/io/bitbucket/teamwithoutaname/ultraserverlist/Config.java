package io.bitbucket.teamwithoutaname.ultraserverlist;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;

public class Config {

	private static String CONFIG_LOCATION = "config.yml";
	private static String SERVER_HASH = "ServerIdentifierHash";
	private static String MASTER_SERVER = "MasterServer";
	
	private static String DEFAULT_CONFIG = 
			MASTER_SERVER+": www.ultraserverlist.com\n"+
			SERVER_HASH+": changeme\n";
	
	private USL plugin;
	
	private String masterServer;
	private String serverIdentifier;
	
	public Config(USL plugin) {
		this.plugin = plugin;
		
		try {
			readConfig();
		} catch(IOException e) {
			plugin.output.severe("Failed to to read configuration file!");
			plugin.output.severe(e.getMessage());
		}
	}
	
	public File getConfigFile() { return new File(plugin.getDataFolder(), CONFIG_LOCATION); }
	public String getMasterServer() { return masterServer; }
	public String getServerIdentifier() { return serverIdentifier; }
	
	private void readConfig() throws IOException {
		File configFile = getConfigFile();
		if(!configFile.exists()) {
			new File(configFile.getParent()).mkdirs();
			createDefaultConfigFile(configFile);
		}
		
		YamlConfiguration configYaml = YamlConfiguration.loadConfiguration(configFile);
		masterServer = configYaml.getString(MASTER_SERVER);
		serverIdentifier = configYaml.getString(SERVER_HASH);
	}
	
	private void createDefaultConfigFile(File configFile) throws IOException {
		byte[] text = DEFAULT_CONFIG.getBytes();
		
		FileOutputStream outputStream = new FileOutputStream(configFile, false);
		outputStream.write(text);
		outputStream.close();
	}
	
}
