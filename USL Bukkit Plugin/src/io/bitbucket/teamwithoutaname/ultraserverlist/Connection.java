package io.bitbucket.teamwithoutaname.ultraserverlist;

import java.net.HttpURLConnection;
import java.net.URL;

import io.bitbucket.teamwithoutaname.ultraserverlist.util.StreamUtil;

public class Connection {

	private static final String API_PATH = "/lib/core/game/minecraft/";
	
	private USL plugin;
	
	public Connection(USL plugin) {
		this.plugin = plugin;
	}
	
	//http://stackoverflow.com/questions/1359689/how-to-send-http-request-in-java
	public boolean pingMasterServer() {
		String masterServer = plugin.config.getMasterServer();
		
		plugin.output.info("Pinging master server (" + masterServer + ")...");
		
		try {
			//Create connection
			URL url = new URL("http://" + masterServer + API_PATH + "ping.php");
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			
			//Get Response
			String response = StreamUtil.convertInputStreamToString(connection.getInputStream());
			
			connection.disconnect();
			
			plugin.output.info("Got ping response from master server(" + response + ")!");
			return true;
			
		} catch (Exception e) {
			plugin.output.info("Failed to ping master server (" + e.getMessage() + ")!");
		}
		
		return false;
	}
	
}
