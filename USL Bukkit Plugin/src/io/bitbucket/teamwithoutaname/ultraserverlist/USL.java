package io.bitbucket.teamwithoutaname.ultraserverlist;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class USL extends JavaPlugin {
	
	public Logger output;
	public Config config;
	public Connection connection;
	
	@Override
	public void onEnable() {
		output = getLogger();
		config = new Config(this);
		connection = new Connection(this);
		
		connection.pingMasterServer();
		
		output.info("USL Bukkit Plugin was successfully enabled");
	}
	
	@Override
	public void onDisable() {
		
		output.info("USL Bukkit Plugin was successfully disabled");
		
	}
	
}
